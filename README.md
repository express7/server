# server

[![npm version](https://img.shields.io/npm/v/express-validator.svg)](https://git.immobisp.com/express/server)
[![Build Status](https://img.shields.io/travis/express-validator/express-validator.svg)](https://git.immobisp.com/express/server)
[![Coverage Status](https://img.shields.io/coveralls/express-validator/express-validator.svg)](https://git.immobisp.com/express/server/-/tree/main)

An [express.js](https://github.com/visionmedia/express) fullstack
[application](https://git.immobisp.com/express/server).

- [Installation](#installation)
- [Documentation](#documentation)
- [Changelog](#changelog)
- [License](#license)

## Installation

Clone boiler plate:

```console
git clone https://git.immobisp.com/express/server.git
```

Run production:

```console
npm run start
```

Run development:

```console
npm run dev
```

View the website at: http://localhost:3000


Automatic code formatting:

```console
npm run format
```

Debugging on linux or mac:

```console
npm run debug
```

Debugging on ms windows:

```console
npm run debugw
```


Also make sure that you have Node.js 8 or newer in order to use it.

## Documentation

Please refer to the documentation website on https://git.immobisp.com/express/server/-/blob/master/README.md.

### Env

Environment from .env file will be loaded into proccess.env by app.
Those enviroments can be accessed using Env function below:

```js
const Env = require('app/env')

const port = Env.get('PORT', /* set default value */ 3000)
```

### Config

All configuration inside config folder can be accessed using dot properties, with function below:

```js
const Config = require('app/config')

const mysql = Config.get('database.mysql.host', /* set default value */ '127.0.0.1')
```

### Database

Example of selecting users:

```js
const Database = require('app/database')

Route.get('/', async () => {
  return await Database.table('users').select('*')
})
```

### Model

Base model:

```js
const Model = require('app/model')
```

Creating model class:

```js
const Model = require('app/model/user')

class User extends Model {
}
```

Example user model usage:

```js
const User = require('models/user')

Route.get('users', async () => {
  return await User.find()
})
```

### Helpers

Paths and lodash functions can be accessed from helpers.
Paths are as following:
  - rootPath
  - appPath
  - publicPath
  - viewPath
  - tmpPath

```js
const _ = require('app/helpers')

const tmp = _.tmpPath()
```

### Routes

Example of default format routes (example.js) will be accessed as GET /api/example/users:

```js
'use strict'

// const debug = require('debug')('app:route:list')
const Database = require('app/database')
const { Router, check, validate, version } = require('app/router')
const createError = require('http-errors')

const router = Router({ mergeParams: true })
const v = version({ default: '1.0.0' })

module.exports = router
  .get('/users.:format?', v('1.0.0'), // version 1.0.0
    check('var1').not().isEmpty().customSanitizer(c => Array.isArray(c) ? c[0] : c), // check and sanitize querystrings
    check('limit').default(15), // check with default value
    validate, // validate all checks

    // control
    async (req, res, next) => {
      try {
        if (req.method !== 'GET' && req.method !== 'POST') return next(createError(404))

        const { var1 } = req.all()

        const query = await Database.raw('SELECT * FROM users')
        const { rows = query } = query

        return res.json(rows)
      } catch (err) {
        return next(createError(500, err))
      }
    }
  )
```

## Changelog

Check the [GitLab Releases page](https://git.immobisp.com/express/server/-/blob/master/CHANGELOG.md).

## License

MIT License
