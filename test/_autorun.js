'use strict'

const fs = require('fs')
const path = require('path')
const _ = require('app/helpers')
// const { rootRequire } = require('app/helpers/path')
const { sql, failedSql, createToken } = require('test')
const mocks = require('./sql.json')

before(async function () {
  // create token
  global.token = await createToken()

  // setting mock
  for (const m of mocks) sql.add(m.sql, m.rows)
})

require('test/internal')

after(async function () {
  // save failed queries
  const json = Object.values(failedSql).map(sql => ({ sql, rows: [{ id: 1 }] }))

  fs.writeFileSync(path.join(_.tmpPath(), 'failed_test_query.json'), JSON.stringify(json, null, 2))
})
