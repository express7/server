'use strict'

const express = require('express')

module.exports = [
  // 'app/middleware/access-log',
  // 'app/middleware/morgan-dev',
  // 'app/middleware/toobusy',
  express.json,
  express.urlencoded,
  'cookie-parser',
  'helmet',
  'cors',
  // 'hpp',
  'compression',
  // 'oas'
]
