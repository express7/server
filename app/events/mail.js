'use strict'

const debug = require('debug')('app:events')
const mail = require('app/mail')

module.exports = app => {
  app.on('auth:signup', async data => {
    try {
      await mail.send({
        to: data.user.email,
        subject: 'User created',
        text: 'Text', 
        html: 'Html'
      })
    } catch (err) {
      debug(err)
    }
  })

  app.on('auth:forgot', async data => {
    try {
      await mail.send({
        to: data.user.email,
        subject: 'Forgot password',
        text: data.token, 
        html: 'Html'
      })
    } catch (err) {
      debug(err)
    }
  })
}
