'use strict'

const path = require('path')
const debug = require('debug')(`route:${path.basename(path.dirname(__dirname))}:index`)
const { Router, auth, cache } = require('app/router')

const router = Router({ mergeParams: true })

debug('route')
router.path = '(/|/api)?'

module.exports = router
  // server health check
  .get('/health(z|check)', async (req, res, next) => res.json({ health: 'ok' }))

  .use('/auth', require('app/auth/route'))

  // with middleware
  .use(
    auth(),
    cache()
  )

  // map routes
  // .use('/map', require('map/route'))

  // sub routes
  .dir()

  // model routes
  .use(require('app/model/route')({ includeSchema: true }))
