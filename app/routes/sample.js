'use strict'

const debug = require('debug')('app:route:sample')
const _ = require('app/helpers')
const Database = require('app/database')
const { Router, check, validate, version } = require('app/router')
const createError = require('http-errors')

const router = Router({ mergeParams: true })
const v = version({ default: '1.0.0' })

const argsArray = true

// READYOK
module.exports = router
  .all('/test.:format?', v('1.0.0'),
    // check('limit').default(15),
    // validate,

    // control
    async (req, res, next) => {
      try {
        if (req.method !== 'GET' && req.method !== 'POST') return next(createError(404))

        const data = 'sample version 1.0.0'

        return res.json({ data })
      } catch (err) {
        return next(createError(500, err))
      }
    }
  )

  .all('/test.:format?', v('2.0.0'),
    // check('limit').default(15),
    // validate,

    // control
    async (req, res, next) => {
      try {
        if (req.method !== 'GET' && req.method !== 'POST') return next(createError(404))

        const data = 'sample version 2.0.0'

        return res.json({ data })
      } catch (err) {
        return next(createError(500, err))
      }
    }
  )