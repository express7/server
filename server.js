#!/usr/bin/env node

'use strict'

const debug = require('debug')('express:server')

if (process.env.npm_lifecycle_event === 'dev') {
  const util = require('util')
  const { defaultOptions: { maxArrayLength, maxStringLength, compact } = {} } = util.inspect
  if (maxArrayLength) util.inspect.defaultOptions.maxArrayLength = 200
  if (maxStringLength) util.inspect.defaultOptions.maxStringLength = 200
  if (compact) util.inspect.defaultOptions.compact = true
}

/**
 * Module dependencies.
 */
const app = require('app')

/**
 * Get host from environment and store in Express.
 */
const host = process.env.KUBERNETES_SERVICE_HOST
  ? '0.0.0.0'
  : (process.env.HOST || '0.0.0.0')

/**
 * Get port from environment and store in Express.
 */
const port = process.env.KUBERNETES_SERVICE_HOST
  ? 8080
  : normalizePort(process.env.PORT || 8080)

app.set('port', port)

/**
 * Create HTTP server.
 */
const { 1: protocol } = (process.env.APP_URL || 'http:').match(/^(.*?):/)
const server = require(protocol).createServer(require('https-pem'), app)

server.on('error', onError)
server.once('listening', onListening)

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, host)

/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val) {
  const port = parseInt(val, 10)

  // named pipe
  if (isNaN(port)) return val

  // port number
  if (port >= 0) return port

  return false
}

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error) {
  if (error.syscall !== 'listen') throw error

  const bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges')
      process.exit(1)
      break
    case 'EADDRINUSE':
      console.error(bind + ' is already in use')
      process.exit(1)
      break
    default:
      throw error
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
  const addr = server.address()

  const bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port

  app.set('ready', true)

  debug('Listening on ' + bind)

  console.log(`App running on ${eval(`\`${process.env.APP_URL}\``)}`)
}
